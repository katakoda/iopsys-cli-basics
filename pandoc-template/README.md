# pandoc-template

Files and scripts for using Pandoc to create PDF from Markdown.

## Prerequisites

In order for conversion to work, you need to also install: 

* [Pandoc](https://pandoc.org/installing.html)

* [Latex](https://www.latex-project.org/get/#tex-distributions) w. Xelatex engine


## Frontmatter

Your markdown file needs to contain YAML frontmatter with variables to include. 

Change according to your needs: 

```yaml
---
title: "iopsysWrt"
subtitle: "Subtitle"
version: "Version "
author: [Iopsys]
date:   9999-99-99
keywords: [iopsysWrt]
... 

```

## Conversion

Use the command below and change inout and output filenames according to your needs: 

* sample.md
* sample.pdf

In the command below, these need to be absolute paths.


### Command: 

```
pandoc --listings --toc --toc-depth=1 --pdf-engine=xelatex --template="tpl/iopsys.latex" --metadata=titlepage:true --variable=logo:tpl/img/iopsys_neg.png --variable=titlepage-background:tpl/img/front.pdf --variable=titlepage-color:000000 --variable=titlepage-text-color:FFFFFF --variable=toc-own-page:true  --variable=toc-own-page:true --variable=logo-width:200 sample.md -o sample.pdf
```


