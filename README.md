
# README

docs-iopsys-release-notes

Source documents and templates for release notes.

## Requirements

For information about the requirements, see `requirements.md`.

## Directories

For information about the repo contents, see `directories.md`.

## Workflow

For information about the repo contents, see `workflow.md`.
