# Release Notes 4.3.4

Purpose
=======

This document provides information about the  4.3.4 maintenance  release, describing issues fixes, known limitations and remaining issues.

Overview
========

The following new features have been added in the IOPSYS SDK release.



New Features
=================


| # |Updated|Area|Subject|
|----|-------|------|----------------------------------------------------------------|
| 1876 | 20200401 | Security | KR00K - CVE-2019-15126                                                |
| 2345 | 20200401 | SDK | genconfig supports cloning both via SSH and HTTPS for building the private SDK |



Issues fixed
================

See below detailed list of corrected issues.


| # |Updated|Area|Subject|
|----|-------|------|----------------------------------------------------------------|
| 1587 | 20200401 | WebGUI   | WiFi: WebGUI WiFi Scan reports wrong SNR values on Broadcom platforms |
| 1548 | 20200401 |Voice    | Voice Brcm: No dialtone set when register new SIP account in web GUI  |
Known issues
======================================

-   System: Mounting EXT3/4 fileystems does not work on Intel platform 


Known Limitations
================

-   WiFi Life operates on 5GHz frequency only

-   WiFi Life is not supported on Intel platforms

-   Easy QoS is supported on Broadcom platforms only

Hardware
===============

### Supported Chipsets

Please find below the list of supported chipsets for iopsysWrt SDK
release  4.3.2:

| SoC Manufacturer | Chipset | Product Type | Target Family | 
|---|---|---|---|
| Broadcom | 63138 | DSL, SFP, Voice+DECT | iopsys-brcm63xx-arm
| Broadcom | 63168 | DSL, Voice | iopsys-brcm63xx-mips  |
| Broadcom | 63268 | DSL, SFP, Voice+DECT | iopsys-brcm63xx-mips  | 
| Intel | GRX350 | DSL, SFP, Voice |  |
| Intel | GRX550 | DSL, SFP, Voice |  |
| MediaTek | MT7621 | WiFi Extender/Repeater |  | 

