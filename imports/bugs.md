| # |Updated|Area|Subject|
|------------------------------|--------|----------|----------------------------------------------------------------------------------------------------------|
| 1587 | 20200401 | WebGUI   | WiFi: WebGUI WiFi Scan reports wrong SNR values on Broadcom platforms |
| 1548 | 20200401 |Voice    | Voice Brcm: No dialtone set when register new SIP account in web GUI  |