# docs-iopsys-release-notes

Source documents and templates for release notes.

## Directories

For information about the repo contents, see `directories.md`.

## Workflow

To create release notes, follow this procedure:

1. Pull the repo

2. Switch to `devel` branch

3. Create a new branch for the version (if it doesn't already exist)
  - **IMPORTANT**: The branch name must match the project version you want to create release notes for.

4. Edit the `manual-texts` files to your liking. 

5. Edit the `issues` files to your liking. 

6. Run the `./utils/build-release-notes.sh` script

7. Commit the changes.

8. Push the repo.


### NOTE

The branch version must match the project version you want to create release notes. **however** anything following the minor version will be lopped off. 

All alphas, betas and other special versions will be treated as part of the version.

* Example: 
  - A branch of `4.4.0` will include *resolved* and *closed* issues for versions `4.4.0*`, for example `4.4.0ALPHA1`, `4.4.0BETA1`, `4.4.4RC1`,  and so on. Anything following the minor version will be ignored and in the match.
  
