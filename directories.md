# docs-iopsys-release-notes

Source documents and templates for release notes.

## Directory structure

* `assets`
  - Various files and data, sample files, old release notes, general miscellanous.

* `build`
  - Directory where the generated release notes file(s) is stored.

* `imports`
  - Directory containing information about the issues collected from Redmine (based on the version indicated by the repo branch).

* `manual`
  - Files that need to be manually modified.

* `pdf`
  - Output directory for PDF:s.
  
* `template`
  - Directory containing the template(s) for file generation, Specifically:
    - `release_notes.md`
      - This is the template for building the release notes file. It consists of file names (for files that must be in the `source` directory).

* `tpl`
  - Directory containing pandoc-related template files and directories:
    - `img`
        - Images and logos needed for the template
    - `pdftemplate.latex`
        - Latex code for the pandoc engine.
    

* `utils`
  - Tools and utility programs.


## Workflow

To create release notes, follow this procedure:

1. Pull the repo

2. Create a new branch
  - **IMPORTANT**: The branch name must match the project version you want to create release notes for.

3. Edit the `source` files to your liking. 

4. Edit the `issues` files to your liking. 

5. Run the `./utils/build-release-notes.sh` script

6. Commit the changes.

7. Push the repo.



### NOTE

The branch version must match the project version you want to create **however** anything following the minor version will be lopped off. 

All alphas, betas and other special versions will be treated as part of the version.

* Example: 
  - A branch of `4.4.0` will include *resolved* and *closed* issues for versions `4.4.0*`, for example `4.4.0ALPHA1`, `4.4.0BETA1`, `4.4.4RC1`,  and so on. Anything following the minor version will be ignored and in the match.