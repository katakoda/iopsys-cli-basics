#!/usr/bin/env bash

# Initial variables

today=$(date '+%Y-%m-%d')
input="../template/license_report.md"
metadatamd=""

# Find current git branch

branch=$(git symbolic-ref --short HEAD)

echo 'Branch:' $branch

version=$(echo $branch | sed 's/.*\([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')

version_u=$(echo $branch | sed 's/\./_/')

printf 'Version: %s\n' $version


# Find targets

declare -a TARGETSARRAY

licensedir="../licenses"

for entry in ${licensedir}/*/
  do
    parentdir="$(basename $(dirname "$entry"))"
    entry=${entry[@]%/}         # This removes the trailing slash on each item
    targets_array+=("$entry")
  done

# Clean build

rm -r ../build/md/lic/*
rm -r ../build/txt/lic/*

mkdir -p ../build/md/lic
mkdir -p ../build/txt/lic

rm -r ../build/pdf/lic/*
mkdir -p ../build/pdf/lic

# Generate for targets

for target in "${targets_array[@]}"
  do
    printf '%s\n' $(basename $target)
    for entry in ${target}/*
      do
        file=$(basename ${entry})
        extension="${file##*.}"
        filename="${file%.*}"
        targets_array+=("$target-$filename")

        # Output variables
            printf "File %s\n" ${filename}

        outputmd="../build/md/lic/${filename}.md"
        outputtxt="../build/txt/lic/${filename}.txt"
        outputpdf="../build/pdf/lic/${filename}.pdf"

      done

    # Create Metadata


    metadatamd="---
title: \"iopsysWrt - License report\"
subtitle: \"${target}\"
version: \"Version ${version}\"
author: [Iopsys]
date:   ${today}
keywords: [iopsysWrt, Release Notes]
...
    "
    # echo "$metadatamd"

    # Use template

    echo "$metadatamd" >> $outputmd

    while IFS= read -r line
    do
      # Skip if empty or comment
      if [[ "$line" ]] && [[ ${line:0:1} != "#" ]]
      then
      #  modify for targets
        if [[ ${line:0:6} != "manual" ]]
        then
          parent="${target[@]%-*}"
          sub="${target[@]##*-}"
          line="licenses/${parent}/${sub}/${line}"
        fi

      # echo "../$line.md"

      # Otherwise, add source and issue files according to the template
         cat "../$line.md" >> $outputmd
         echo "" >> $outputmd
      # create wiki file
         cat "../$line.md" >> $outputtxt
         echo "" >> $outputtxt
       fi
    done < "$input"


    pandoc --listings --toc --toc-depth=3 --pdf-engine=xelatex --template="../pandoc-template/tpl/iopsys.latex" --metadata=titlepage:true --variable=logo:../pandoc-template/tpl/img/iopsys_neg.png --variable=titlepage-background:../pandoc-template/tpl/img/front.pdf --variable=titlepage-color:000000 --variable=titlepage-text-color:FFFFFF --variable=toc-own-page:true  --variable=toc-own-page:true --variable=logo-width:240 --variable=titlepage-rule-color:FFFFFF  ${outputmd} -o ${outputpdf}

done