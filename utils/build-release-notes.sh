#!/usr/bin/env bash

# Initial variables

today=$(date '+%Y-%m-%d')
input="../template/release_notes.md"
metadatamd=""

# Use passed branch name or
# Find current git branch

if ! [ $1 == '' ]
then
  branch=$1
else
  branch=$(git symbolic-ref --short HEAD)
fi

echo 'Branch:' $branch

version=$(echo $branch | sed 's/.*\([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')
version_u=$(echo $branch | sed 's/\./_/')

echo 'Version:' $version

# Output variables

outputmd="../build/md/rn/iopsyswrt_release_notes_${version}.md"
outputtxt="../build/txt/rn/iopsyswrt_release_notes_${version}.txt"
outputpdf="../build/pdf/rn/iopsyswrt_release_notes_${version}.pdf"

echo 'outputpdf:' $outputpdf
echo 'outputmd:' $outputmd


# Clean build

rm -r ../build/md/rn/*
rm -r ../build/txt/rn/*

mkdir -p ../build/md/rn
mkdir -p ../build/txt/rn

touch $outputmd
touch $outputtxt

rm -r ../build/pdf/rn/*
mkdir -p ../build/pdf/rn

# Create Metadata 

metadatamd="---
title: \"iopsysWrt\"
subtitle: \"Release Notes\"
version: \"Version ${version}\"
author: [Iopsys]
date:   ${today}
keywords: [iopsysWrt, Release Notes]
... 
"
echo "$metadatamd"

# Use template

touch $outputmd
touch $outputtxt

echo "$metadatamd" >> $outputmd

echo "# Release Notes ${branch}
" >> $outputmd

echo "# Release Notes ${branch}
" >> $outputtxt

while IFS= read -r line
do
  # Skip if empty or comment
  if [[ "$line" ]] && [[ ${line:0:1} != "#" ]]
  then
  # Otherwise, add source and issue files according to the template
     echo "../$line.md"
     cat "../$line.md" >> $outputmd
     echo "" >> $outputmd
  # create wiki file
     echo "../$line.md"
     cat "../$line.md" >> $outputtxt
     echo "" >> $outputtxt
   fi
done < "$input"


# Use Pandoc and tpl to convert

pandoc --listings --toc --toc-depth=1 --pdf-engine=xelatex --template="../pandoc-template/tpl/iopsys.latex" --metadata=titlepage:true --variable=logo:../pandoc-template/tpl/img/iopsys_neg.png --variable=titlepage-background:../pandoc-template/tpl/img/front.pdf --variable=titlepage-color:000000 --variable=titlepage-text-color:FFFFFF --variable=toc-own-page:true  --variable=toc-own-page:true --variable=logo-width:100 ${outputmd} -o ${outputpdf}

