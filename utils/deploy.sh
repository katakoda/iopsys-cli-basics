#!/usr/bin/env bash

# Initial variables

today=$(date '+%Y-%m-%d')

# Find current git branch

branch=$(git symbolic-ref --short HEAD)

echo 'Branch:' $branch

version=$(echo $branch | sed 's/.*\([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/')
version_u=$(echo $branch | sed 's/\./_/g')
version_maj_u=${version_u%_*}
sort=$(echo $branch | sed 's/\.//g')

echo 'Version:' $version

# Create wikipage content

wikipage="{{indexmenu_n>$sort}}

# $version

Release related information

##

<nspages -h1 -hideNoPages -hideNoSubns -textPages=\"\" -sortId -textNS=\"\" -exclude -subns>

## Downloads

![Release Notes $version](./iopsyswrt_release_notes_$version.pdf)

"

# Input variables

mediasrc="../build/pdf/rn/iopsyswrt_release_notes_$version.pdf"
pagesrc="../build/txt/rn/iopsyswrt_release_notes_$version.txt"
buildpath="../build"

# Output variables

pagepath="/wiki/pages/sdk/releases/${version_maj_u}/${version_u}"
mediapath="/wiki/media/sdk/releases/${version_maj_u}/${version_u}"
repopath="../tmp/git/internal"

# Clean tmp directory
# rm -r ../tmp/*


# get internal repo
# git clone -b devel --single-branch git@dev.iopsys.eu:docs/docs-iopsys-internal.git  $repopath/internal


# update internal repo
git -C $repopath pull


# Create paths

mkdir -p $repopath$pagepath
mkdir -p $repopath$mediapath

# Create wiki start file

echo "$wikipage" > $repopath$pagepath/start.txt


# Copy pages and pdf
cp $mediasrc $repopath/$mediapath/

cp $pagesrc $repopath/$pagepath/


# Commit changes and push

git -C $repopath commit -m "Update $today"
git -C $repopath push


