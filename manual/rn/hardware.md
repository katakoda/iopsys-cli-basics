Hardware
===============

### Supported Chipsets

Please find below the list of supported chipsets for iopsysWrt SDK
release  4.3.2:

| SoC Manufacturer | Chipset | Product Type | Target Family | 
|---|---|---|---|
| Broadcom | 63138 | DSL, SFP, Voice+DECT | iopsys-brcm63xx-arm
| Broadcom | 63168 | DSL, Voice | iopsys-brcm63xx-mips  |
| Broadcom | 63268 | DSL, SFP, Voice+DECT | iopsys-brcm63xx-mips  | 
| Intel | GRX350 | DSL, SFP, Voice |  |
| Intel | GRX550 | DSL, SFP, Voice |  |
| MediaTek | MT7621 | WiFi Extender/Repeater |  | 
