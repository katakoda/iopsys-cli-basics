Known Limitations
================

-   WiFi Life operates on 5GHz frequency only

-   WiFi Life is not supported on Intel platforms

-   Easy QoS is supported on Broadcom platforms only
