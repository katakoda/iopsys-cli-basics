---------------------------------
Release Notes for iopsysWrt SDK
---------------------------------

![](./media/image1.jpg)

![](./media/image2.emf)


Purpose
=======

This Document is intended to support the software release *\<xxxxx\>*
with new functionalities, issues fixes, as well as know issues
descriptions.

Overview
========

*Insert here the new functionalities description, added for this release
(in same fashion as it was done in 4.2.0 release notes)*

2.1 Reference documentation
---------------------------

*Link to test spec and test report with file names and dates*

  1.   Iopsyswrt test specification   Yyyy/mm/dd
  ---- ------------------------------ ------------
  2.   Iopsyswrt test report          Yyyy/mm/dd


2.2 New feature 1
-----------------

*blablabla*

2.2 New feature 2
-----------------

*blablabla*

New Features List
=================

*Extract from Redmine, as a table with ticket number and RN Extra info
(to be decided if we keep subject or not)*

Issue fixes List
================

*Extract from Redmine, as a table with ticket number and RN Extra info
(to be decided if we keep subject or not)*

Remaining Issues List (or know issues)
======================================

*Insert here the know issues, for this release (in same fashion as it
was done in 4.2.0 release notes)*

*[To be decided if we list the remaining bugs, or just a synthetized
list like done in 4.2.0 release notes.]{.underline}*

Known Limitation
================

*Insert here the new functionalities description, added for this release
(in same fashion as it was done in 4.2.0 release notes)*

6.1 Know Limitation 1
---------------------

*blablabla*

6.2 Know Limitation 2
---------------------

*Blablabla*

Tested Hardware
===============

*Insert here list of hardware tested during development and delivery (from test report & test spec.)*
