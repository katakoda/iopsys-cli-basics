# Requirements

To run pdf conversion, Pandoc is required, as well as Latex (xelatex engine) for conversion.

These commands ought to do it in Debian:

```
sudo apt-get install pandoc

sudo apt-get install texlive
```

## More information 

[https://pandoc.org/installing.html]()

[https://tug.org/texlive/]()

